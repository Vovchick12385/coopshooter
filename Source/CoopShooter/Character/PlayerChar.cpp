// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerChar.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/PawnMovementComponent.h"
// Sets default values
APlayerChar::APlayerChar()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ManeCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	ManeCamera->bUsePawnControlRotation = true;
	ManeCamera->SetupAttachment(RootComponent);


	const ConstructorHelpers::FObjectFinder<USkeletalMesh>New_Mesh(TEXT("/Game/Assets/AnimStarterPack/UE4_Mannequin/Mesh/SK_Mannequin"));
	this->GetMesh()->SetSkeletalMesh(New_Mesh.Object);
	
	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true;

}
// Called when the game starts or when spawned
void APlayerChar::BeginPlay()
{
	Super::BeginPlay();
	
}

void APlayerChar::MoveForward(float amount)
{
	AddMovementInput(GetActorForwardVector() * amount);
}

void APlayerChar::MoveRight(float amount)
{
	AddMovementInput(GetActorRightVector() * amount);
}

void APlayerChar::Fire()
{
}

void APlayerChar::StopFire()
{
}

void APlayerChar::ChangeWeapon(float amount)
{
}

void APlayerChar::DoSomeThing()
{
}

void APlayerChar::TakeAim()
{
}

void APlayerChar::StopTakeAim()
{
}

void APlayerChar::BeginCrouch()
{
	Crouch();
}

void APlayerChar::EndCrouch()
{
	UnCrouch();
}

//void APlayerChar::BeginLie()
//{
//	Lie();
//}
//
//void APlayerChar::EndLie()
//{
//}

// Called every frame
void APlayerChar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerChar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerChar::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerChar::MoveRight);
	PlayerInputComponent->BindAxis("LookUp", this, &APlayerChar::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn", this, &APlayerChar::AddControllerYawInput);
	PlayerInputComponent->BindAxis("ScrollingWeapon", this, &APlayerChar::ChangeWeapon);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &APlayerChar::Fire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &APlayerChar::StopFire);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("TakeAim", IE_Pressed, this, &APlayerChar::TakeAim);
	PlayerInputComponent->BindAction("TakeAim", IE_Released, this, &APlayerChar::StopTakeAim);
	PlayerInputComponent->BindAction("DoSomeThing", IE_Pressed, this, &APlayerChar::DoSomeThing);

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &APlayerChar::BeginCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &APlayerChar::EndCrouch);
	/*PlayerInputComponent->BindAction("Lie", IE_Pressed, this, &APlayerChar::BeginLie);
	PlayerInputComponent->BindAction("Lie", IE_Released, this, &APlayerChar::EndLie);*/
}


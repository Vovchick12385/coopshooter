// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerChar.generated.h"


class UCameraComponent;


UCLASS()
class COOPSHOOTER_API APlayerChar : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerChar();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveForward(float amount);
	
	void MoveRight(float amount);
	void Fire();
	void StopFire();
	void ChangeWeapon(float amount);
	void DoSomeThing();
	void TakeAim();
	void StopTakeAim();
	
	void BeginCrouch();
	void EndCrouch();
	/*void BeginLie();
	void EndLie();*/

	UPROPERTY(VisibleAnywhere,BluePrintReadOnly, Category = "Camera")
		UCameraComponent* ManeCamera;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	
	
};
